package utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * Created by user on 09.04.2017.
 */
public class FileGenerator {

    private static final String mCHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    private static final int STR_LENGTH = 9; // длина генерируемой строки
    public static final int FILE_LENGTH = 10000; // длина file
    private static Random random = new Random();

    public static String createRandomString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < STR_LENGTH; i++) {
            int number = random.nextInt(mCHAR.length());
            char ch = mCHAR.charAt(number);
            builder.append(ch);
        }
        return builder.toString();
    }
    public static void writeFile(File file) {
        try (FileWriter writer = new FileWriter(file)) {
            for (int i = 0; i < FILE_LENGTH; i++) {
                writer.write(Integer.toString(i));
                writer.write("-");
                writer.write(createRandomString());
                writer.write("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
