package cache;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by arty on 22.05.2018.
 */
public class Cache implements Cacheable {

    private Map<String, String> cache = new HashMap<>();
    private File file;

    public Cache() {
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Map<String, String> getCache() {
        return cache;
    }

    @Override
    public String get(String key) {
        return cache.get(key);
    }

    @Override
    public void put(String key, String value) {
        cache.put(key, value);
    }

    @Override
    public int size() {
        return cache.size();
    }

    @Override
    public void clear() {
        cache.clear();
    }


    public void fillCache() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (scanner.hasNext()) {
            String str = scanner.nextLine();
            String[] tmp = str.split("-");
            cache.put(tmp[0], tmp[1]);
        }
    }
}
