package cache;

import utils.FileGenerator;

import java.io.File;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by arty on 26.05.2018.
 */
public class ThreadPooler {

    public AtomicInteger finalCountDown = new AtomicInteger(0);
    public AtomicInteger counter = new AtomicInteger(0);
    private final static int REQUEST_COUNT = 99;

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock writeLock = readWriteLock.writeLock();
    private final Lock readLock = readWriteLock.readLock();
    private static Random random = new Random();

    private final static int THREAD_QUANTITY = 1000;

    private Cache cache;

    public ThreadPooler() {
    }

    public void setCache(Cache cache) {
        this.cache = cache;
    }

    public void createThreadPool() {
        ExecutorService service = Executors.newFixedThreadPool(THREAD_QUANTITY);
        startExecution(service);
    }

    public void startExecution(ExecutorService service) {
        for (int i = 0; i < THREAD_QUANTITY; i++) {
            try {
                service.execute(new Thread(() -> readCacheByThread(), "thread#" + i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        service.shutdown();
        System.out.println("Finish!");
        System.out.println("Refill counter :" + counter);
    }

    public void readCacheByThread() {
        String key = "";
        //for (int i = 0; i < 10; i++) {
            key = Integer.toString(
                    random.nextInt((FileGenerator.FILE_LENGTH) + 1));
            if (finalCountDown.get() >= REQUEST_COUNT) {
                refillCache(key);
            } else {
                readCache(key);
            }
      //  }
    }

    private void readCache(String key) {
            if (finalCountDown.get() < REQUEST_COUNT) {
                String tmp = cache.get(key);
                finalCountDown.addAndGet(1);
                if (tmp == null) {
                    System.out.println();
                    //System.out.println("It's not good!");
                }
                System.out.println(Thread.currentThread().getName() + "# " + finalCountDown);
            } else {
                refillCache(key);
            }
    }

    private void refillCache(String key) {
        writeLock.lock();
        try {
            if (finalCountDown.get() >= REQUEST_COUNT) {
                cache.clear();
                cache.fillCache();
                finalCountDown.getAndSet(0);
                System.out.println();
                System.out.println("Stop reading, cache restart");
                System.out.println("Cache refill");
                counter.addAndGet(1);
            } else {
                readCache(key);
            }
        } finally {
            writeLock.unlock();
        }
    }
}
