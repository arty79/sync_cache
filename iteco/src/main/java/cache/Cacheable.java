package cache;

/**
 * Created by arty on 22.05.2018.
 */
public interface Cacheable {
    String get(String key);

    void put(String key, String value);

    int size();

    void clear();
}
