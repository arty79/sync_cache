/**
 * Created by arty on 22.05.2018.
 */

import cache.Cache;
import cache.ThreadPooler;
import utils.FileGenerator;

import java.io.*;


public class Main {

    private static String filePath;

    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Введите путь к файлу");
            //filePath = reader.readLine();
            filePath = new String("c:\\file.txt");
            File file = new File(filePath);
            FileGenerator.writeFile(file);
            if (file.exists() && !file.isDirectory()) {
                Cache cache = new Cache();
                cache.setFile(file);
                cache.fillCache();
                ThreadPooler tp = new ThreadPooler();
                tp.setCache(cache);
                tp.createThreadPool();
            } else {
                System.out.println("По указанному пути не удалось найти файл");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
