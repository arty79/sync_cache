import cache.Cache;
import cache.ThreadPooler;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import utils.FileGenerator;

import java.io.File;

import static org.mockito.Mockito.*;

/**
 * Created by arty on 27.05.2018.
 */

public class CacheTest {
    @Mock
    private Cache cache;
    @InjectMocks
    private ThreadPooler threadPooler;

    @Before
    public void setUp() {
        File file = new File("c:\\file.txt");
        FileGenerator.writeFile(file);
        MockitoAnnotations.initMocks(this);
        cache.setFile(file);
        cache.fillCache();
    }

    @Test
    public void testCache() {
        threadPooler.createThreadPool();
        verify(cache, atLeast(5)).fillCache();
    }


}
